## Learned SPNs

Repository for SPNs learned with the PySPN app: https://gitlab.com/pgm-usp/pyspn
Additionally, SPNs learned with the rust port of the PySPN app: https://gitlab.com/marcheing/spn-rs

### Notes

- US Census was learned with only a part of the Dataset, given its size. The dataset was divided
into 50 parts, and an SPN was learned for each of the 10 first parts, being united into a Root Sum
Node with uniform weights to form the SPN in the file.
- Optdigits was learned by subdividing the datasets into 10 parts, each corresponding to a
classification, and then united into a Root Sum Node with uniform weights to form the SPN in the
file.

### SPN Information

SPN Name | Nodes | Variables | log2(Problem Size)
-- | -- | -- | --
DNA | 21094 | 180 | 180 
Molecular Biology Promoters | 2854 | 58 | 115
Mushrooms | 5753 | 112 |  112
Nips | 9066 | 500 | 500
Nltcs | 1773 | 16 | 16
Optdigits | 2720 | 65 | 264.91
USCensus | 162118 | 68 | 155.78
Voting | 147638 | 1359 | 1359

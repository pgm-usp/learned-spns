## SPN molecular-biology_promoters

Size of Training Dataset: 75

Size of Test Dataset: 31

Parameters for Learn-SPN:
* Random Seed: 1337
* Number of Clusters: 3
* P-Value for independence testing: 0.5

Scope Size:
* Var 0 -> 4 categories
* Var 1 -> 4 categories
* Var 2 -> 4 categories
* Var 3 -> 4 categories
* Var 4 -> 4 categories
* Var 5 -> 4 categories
* Var 6 -> 4 categories
* Var 7 -> 4 categories
* Var 8 -> 4 categories
* Var 9 -> 4 categories
* Var 10 -> 4 categories
* Var 11 -> 4 categories
* Var 12 -> 4 categories
* Var 13 -> 4 categories
* Var 14 -> 4 categories
* Var 15 -> 4 categories
* Var 16 -> 4 categories
* Var 17 -> 4 categories
* Var 18 -> 4 categories
* Var 19 -> 4 categories
* Var 20 -> 4 categories
* Var 21 -> 4 categories
* Var 22 -> 4 categories
* Var 23 -> 4 categories
* Var 24 -> 4 categories
* Var 25 -> 4 categories
* Var 26 -> 4 categories
* Var 27 -> 4 categories
* Var 28 -> 4 categories
* Var 29 -> 4 categories
* Var 30 -> 4 categories
* Var 31 -> 4 categories
* Var 32 -> 4 categories
* Var 33 -> 4 categories
* Var 34 -> 4 categories
* Var 35 -> 4 categories
* Var 36 -> 4 categories
* Var 37 -> 4 categories
* Var 38 -> 4 categories
* Var 39 -> 4 categories
* Var 40 -> 4 categories
* Var 41 -> 4 categories
* Var 42 -> 4 categories
* Var 43 -> 4 categories
* Var 44 -> 4 categories
* Var 45 -> 4 categories
* Var 46 -> 4 categories
* Var 47 -> 4 categories
* Var 48 -> 4 categories
* Var 49 -> 4 categories
* Var 50 -> 4 categories
* Var 51 -> 4 categories
* Var 52 -> 4 categories
* Var 53 -> 4 categories
* Var 54 -> 4 categories
* Var 55 -> 4 categories
* Var 56 -> 4 categories
* Var 57 -> 2 categories

MAP problem size: 41538374868278621028243970633760768

Number of nodes: 11815

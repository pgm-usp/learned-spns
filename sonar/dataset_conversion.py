import os
import random
import sys
import re

original_dataset_filepath = sys.argv[1]
train_dataset_filepath = sys.argv[2]
test_dataset_filepath = sys.argv[3]

random.seed(1337)

all_data = []
with open(original_dataset_filepath, "r") as dataset_file:
    for line in dataset_file.readlines():
        if line.startswith('var'):
            continue
        all_data.append(re.split('[ ,]', line.strip()))

index_to_labels = {}
for i in range(len(all_data[0])):
    index_to_labels[i] = []

for data_instance in all_data:
    for index, data_point in enumerate(data_instance):
        if data_point not in index_to_labels[index]:
            index_to_labels[index].append(data_point)

train_dataset_size = int(4 * len(all_data) / 5)
test_dataset_size = len(all_data) - train_dataset_size

random.shuffle(all_data)

train_data = all_data[:train_dataset_size]
test_data = all_data[train_dataset_size:]

with open(train_dataset_filepath, "w") as new_dataset_file:
    for var_id, labels in index_to_labels.items():
        line = f"var {var_id} {len(labels)}\n"
        new_dataset_file.write(line)
    for data_instance in train_data:
        line = " ".join(
            [
                str(index_to_labels[var_index].index(var))
                for var_index, var in enumerate(data_instance)
            ]
        )
        line += "\n"
        new_dataset_file.write(line)


with open(test_dataset_filepath, "w") as new_dataset_file:
    for var_id, labels in index_to_labels.items():
        line = f"var {var_id} {len(labels)}\n"
        new_dataset_file.write(line)
    for data_instance in test_data:
        line = " ".join(
            [
                str(index_to_labels[var_index].index(var))
                for var_index, var in enumerate(data_instance)
            ]
        )
        line += "\n"
        new_dataset_file.write(line)
